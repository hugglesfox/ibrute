#ifndef SERIAL_H
#define SERIAL_H

#include <avr/io.h>
#include <string.h>
#include <util/setbaud.h>

/* Initalise the serial controller */
void serial_init(void);

/* Recieve a byte over the serial bus */
unsigned char serial_read(void);

/* Send a message over the serial bus */
void serial_write(char *msg);

#endif  // SERIAL_H
