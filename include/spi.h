#ifndef SPI_H
#define SPI_H

#include <avr/io.h>

/* Initalise the SPI controller */
void spi_init(void);

/* Enable slave SPI communication */
void spi_slave_enable(void);

/* Disable slave SPI communication */
void spi_slave_disable(void);

/* Send data over the SPI bus */
void spi_write(unsigned char data);

/* Receive data over the SPI bus */
unsigned char spi_read();

#endif  // SPI_H
