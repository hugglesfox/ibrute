#ifndef USB_H
#define USB_H

#include "spi.h"
#include <avr/io.h>
#include <stddef.h>

/* Initalise the USB controller */
void usb_init(void);

#endif  // USB_H
