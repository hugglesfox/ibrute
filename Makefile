OUT=src/ibrute
TARGET=atmega328p

SERIAL_BAUD=9600
CPU_FREQ=16000000UL  # 16MHz

PORT?=/dev/ttyUSB0

export TARGET

.PHONY: build clean flash

build:
	$(MAKE) -C src

clean:
	$(MAKE) -C src clean 

flash:
	avrdude -p $(TARGET) -P $(PORT) -c arduino -U flash:w:$(OUT)

