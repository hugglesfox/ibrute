#include "usb.h"

// Write a byte to a register
// Format: 5 bit register number + 0 + r/w + 0,  data
void register_write(unsigned char reg, unsigned char data)
{
	spi_slave_enable();

	spi_write((reg << 3) | 2);
	spi_write(data);

	spi_slave_disable();
}

// Write 32 bits to one of the bus fifo shift registers
void fifo_write(unsigned char reg, unsigned long data)
{
	spi_slave_enable();

	spi_write((reg << 3) | 2);

	for (size_t i = 3; i >= 0; i--)
	{
		spi_write((data >> (i * 8)) & 0xFF);
	}

	spi_slave_disable();
}


// Read a given register
char register_read(unsigned char reg)
{
	spi_slave_enable();

	spi_write(reg << 3);
	char data = spi_read();
	
	spi_slave_disable();

	return data;
}

void usb_reset(void)
{
	// Initiate reset
	register_write(15, 0b001000000);

	// Clear reset
	register_write(15, 0);

	// Wait for PLL to stablise
	while(!(register_read(13) & 1));
}

void usb_init(void)
{
	// Make sure spi controller is enabled
	spi_init();

	// Make sure interrupt pin is an input
	DDRB &= ~(1 << DDB1);

	// Enable full duplex spi, level interrupt and set GPXB to 1 to monitor bus activity
	register_write(17, 0b00011010);

	// Reset chip to power on
	usb_reset();
}
