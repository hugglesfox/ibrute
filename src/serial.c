#include "serial.h"

void serial_init(void)
{
	// Configure 8bit data, 1 stop bit
	UCSR0C |= 3 << UCSZ00;

	// Enable USART0 transmitter and recieverUD 9600
	UCSR0B |= (1 << TXEN0) | (1 << RXEN0);

	// Set the baud rate
	UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE;
}

void serial_write(char *msg)
{
	for (size_t i = 0; i < strlen(msg); i++) {
		// Wait for USART to be ready to send
		while (!(UCSR0A & (1 << UDRE0)));

		// Send character
		UDR0 = msg[i];
	}
}

unsigned char serial_read(void)
{
	// Wait for data
	while  (!(UCSR0A & (1 << RXC0)));
	return UDR0;
}

