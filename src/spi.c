#include "spi.h"

void spi_slave_enable(void)
{
	// Drive SS low
	PORTB &= ~(1 << PORTB2);
}

void spi_slave_disable(void)
{
	// Drive SS high
	PORTB |= 1 << PORTB2;
}

void spi_init(void)
{
	spi_slave_disable();

	// Set SCK, MOSI and SS pins as output
	DDRB |= (1 << DDB5) | (1 << DDB3) | (1 << DDB2);

	// Enable SPI as bus master
	SPCR |= (1 << SPE) | (1 << MSTR);
}

void spi_write(unsigned char data)
{
	// Transmit data
	SPDR = data;

	// Wait for SPI transmission to complete
	while (!(SPSR & (1 << SPIF)));
}

unsigned char spi_read()
{
	// Wait for data
	while(!(SPSR & (1 << SPIF)));

	return SPDR;
}

